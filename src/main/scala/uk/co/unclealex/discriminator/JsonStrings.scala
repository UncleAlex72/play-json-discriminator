package uk.co.unclealex.discriminator

import play.api.libs.json._

/**
  * An object to make it easier to write JSON serialisers and deserialisers for sealed traits whose subclasses
  * can all be represented by JSON a string. For the rest of this documentation, assume the following code exists:
  * {{{
  *   import play.api.libs.json._
  *
  *   sealed trait Bool {
  *     val token: String
  *   }
  *
  *   object Bool {
  *
  *     class BoolImpl(override val token: String) extends Bool
  *
  *     object True extends BoolImpl("TRUE")
  *     object False extends BoolImpl("FALSE")
  *
  *     val findByToken: String => Option[Bool] = token => Seq(True, False).find(_.token == token)
  *   }
  * }}}
  */
object JsonStrings {

  /**
    * Generate an Writes for a sealed trait. For example:
    * {{{
    *   implicit val writes: Writes[Bool] = JsonStrings.writes(_.token)
    * }}}
    *
    * @param tokenExtractor A function that turns the object into a token.
    * @tparam V The type of the object.
    * @return A writes that can write JSON objects.
    */
  def writes[V](tokenExtractor: V => String): Writes[V] = (v: V) => {
    Json.toJson(tokenExtractor(v))
  }

  /**
    * Generate an Reads for a sealed trait. For example:
    * {{{
    *   implicit val reads: Reads[Bool] = JsonStrings.reads(Bool.findByToken)
    * }}}
    *
    * @param builder A function that will attempt to build an object from a string.
    * @tparam V The type of the object.
    * @return A reads that can read JSON objects.
    */
  def reads[V](builder: String => Option[V]): Reads[V] = (json: JsValue) => {
    def readsResultBuilder(token: String): JsResult[V] = {
      builder(token) match {
        case Some(value) => JsSuccess(value)
        case None => JsError(JsonValidationError(s"'$token' is not a valid token"))
      }
    }
    json.validate[String].flatMap(readsResultBuilder)
  }

  /**
    * Generate a Format for a sealed trait. For example:
    * {{{
    *   implicit val format: Format[Bool] = JsonStrings.format(Bool.findByToken, _.token)
    * }}}
    *
    * @param builder A function that will attempt to build an object from a string.
    * @param tokenExtractor A function that turns the object into a token.
    * @tparam V The type of the object.
    * @return A format that can read and write JSON objects.
    */
  def format[V](builder: String => Option[V],
                tokenExtractor: V => String): Format[V] = {
    val _reads = reads(builder)
    val _writes = writes(tokenExtractor)
    Format(_reads, _writes)
  }
}

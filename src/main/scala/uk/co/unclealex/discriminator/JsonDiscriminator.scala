package uk.co.unclealex.discriminator

import play.api.libs.json._

/**
  * An object to make it easier to write JSON serialisers and deserialisers for sealed traits whose subclasses
  * can all be represented by JSON using a field whose value is unique for each subclass. For the documentation in
  * this class, assume the following have been defined:
  * {{{
  *   import play.api.libs.json._
  *
  *   sealed trait Choice
  *   case class FirstChoice(first: String) extends Choice
  *   case class SecondChoice(second: String) extends Choice
  *
  *   val firstChoiceWrites: OWrites[FirstChoice] = Json.writes[FirstChoice]
  *   val secondChoiceWrites: OWrites[SecondChoice] = Json.writes[SecondChoice]
  *   val firstChoiceReads: Reads[FirstChoice] = Json.reads[FirstChoice]
  *   val secondChoiceReads: Reads[SecondChoice] = Json.reads[SecondChoice]
  * }}}
  */
object JsonDiscriminator {

  /**
    * Generate an OWriter for a sealed trait. For example:
    * {{{
    *   implicit val choiceWrites: OWrites[Choice] = JsonDiscriminator.writes("type") {
    *     case fc: FirstChoice => "first" -> firstChoiceWrites.writes(fc)
    *     case sc: SecondChoice => "second" -> secondChoiceWrites.writes(sc)
    *   }
    * }}}
    * @param discriminatorFieldName The name of the field to be used as a discriminator.
    * @param writesBuilder A builder that builds a writer given the type of the object being written.
    * @tparam V The type of object being written.
    * @return A writer that will write the object as JSON and add a discriminator field.
    */
  def writes[V](discriminatorFieldName: String)(writesBuilder: V => (String, JsObject)): OWrites[V] = (v: V) => {
    val (discriminatorFieldValue, jsObj) = writesBuilder(v)
    jsObj + (discriminatorFieldName -> JsString(discriminatorFieldValue))
  }

  /**
    * Generate a Reader for a sealed trait. For example:
    * {{{
    *   implicit val choiceReads: Reads[Choice] = JsonDiscriminator.reads("type") {
    *     case "first" => firstChoiceReads.reads
    *     case "second" => secondChoiceReads.reads
    *   }
    * }}}
    *
    * @param discriminatorFieldName The name of the field to be used as a discriminator.
    * @param readsBuilder A function that takes a string and returns a function that reads a JSON object and returns a
    *                     JsResult.
    * @tparam V The type of the object to be read.
    * @return A Reads that reads a JSON object and looks at the value of a discriminator field to work out
    *         which type of object should be read.
    */
  def reads[V](discriminatorFieldName: String)
              (readsBuilder: PartialFunction[String, JsObject => JsResult[V]]): Reads[V] = (json: JsValue) => {
    def readsResultBuilder(discriminatorFieldValue: String)(jsObj: JsObject): JsResult[V] = {
      readsBuilder.lift(discriminatorFieldValue).map(_.apply(jsObj)).getOrElse {
        JsError(JsonValidationError(s"'$discriminatorFieldValue' is not a valid discriminator value"))
      }
    }

    for {
      jsObj <- json.validate[JsObject]
      discriminatorFieldValue <- (jsObj \ discriminatorFieldName).validate[String]
      result <- readsResultBuilder(discriminatorFieldValue)(jsObj)
    } yield {
      result
    }

  }

  /**
    * Generate an OFormat for a sealed trait. For example:
    * {{{
    *   implicit val choiceFormat: OFormat[Choice] = JsonDiscriminator.format("type") ({
    *     case "first" => firstChoiceReads.reads
    *     case "second" => secondChoiceReads.reads
    *   }, {
    *     case fc: FirstChoice => "first" -> firstChoiceWrites.writes(fc)
    *     case sc: SecondChoice => "second" -> secondChoiceWrites.writes(sc)
    *   })
    * }}}
    *
    * @param discriminatorFieldName The name of the field to be used as a discriminator.
    * @param readsBuilder A function that takes a string and returns a function that reads a JSON object and returns a
    *                     JsResult.
    * @param writesBuilder A builder that builds a writer given the type of the object being written.
    * @tparam V The type of object being written.
    * @return A format that combines a reader and writer.
    */
  def format[V](discriminatorFieldName: String)
               (readsBuilder: PartialFunction[String, JsObject => JsResult[V]],
                writesBuilder: V => (String, JsObject)): OFormat[V] = {
    val _reads = reads(discriminatorFieldName)(readsBuilder).reads _
    val _writes = writes(discriminatorFieldName)(writesBuilder).writes _
    OFormat(_reads, _writes)
  }
}

package uk.co.unclealex.discriminator

import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json._

class JsonStringsSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  sealed trait Bool {
    val token: String
  }

  object Bool {

    class BoolImpl(override val token: String) extends Bool

    object True extends BoolImpl("TRUE")
    object False extends BoolImpl("FALSE")

    val findByToken: String => Option[Bool] = token => Seq(True, False).find(_.token == token)
  }

  "A reads" should {
    implicit val reads: Reads[Bool] = JsonStrings.reads(Bool.findByToken)
    "read a true" in {
      Json.fromJson[Bool](JsString("TRUE")) should ===(JsSuccess(Bool.True))
    }
    "read a false" in {
      Json.fromJson[Bool](JsString("FALSE")) should ===(JsSuccess(Bool.False))
    }
    "not read any other string" in {
      Json.fromJson[Bool](JsString("HELLO")).isSuccess should ===(false)
    }
    "not read objects" in {
      Json.fromJson[Bool](Json.obj()).isSuccess should ===(false)
    }
  }

  "A writes" should {
    implicit val writes: Writes[Bool] = JsonStrings.writes(_.token)
    "write a true" in {
      Json.toJson(Bool.True) should === (JsString("TRUE"))
    }
    "write a false" in {
      Json.toJson(Bool.False) should === (JsString("FALSE"))
    }
  }

  "A format" should {
    implicit val format: Format[Bool] = JsonStrings.format(Bool.findByToken, _.token)
    "read a true" in {
      Json.fromJson[Bool](JsString("TRUE")) should ===(JsSuccess(Bool.True))
    }
    "read a false" in {
      Json.fromJson[Bool](JsString("FALSE")) should ===(JsSuccess(Bool.False))
    }
    "not read any other string" in {
      Json.fromJson[Bool](JsString("HELLO")).isSuccess should ===(false)
    }
    "not read objects" in {
      Json.fromJson[Bool](Json.obj()).isSuccess should ===(false)
    }
    "write a true" in {
      Json.toJson(Bool.True) should === (JsString("TRUE"))
    }
    "write a false" in {
      Json.toJson(Bool.False) should === (JsString("FALSE"))
    }
  }
}

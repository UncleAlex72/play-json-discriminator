package uk.co.unclealex.discriminator

import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json._

class JsonDiscriminatorSpec extends WordSpec with Matchers {

  sealed trait Choice
  case class FirstChoice(first: String) extends Choice
  case class SecondChoice(second: String) extends Choice

  val firstChoiceWrites: OWrites[FirstChoice] = Json.writes[FirstChoice]
  val secondChoiceWrites: OWrites[SecondChoice] = Json.writes[SecondChoice]
  val firstChoiceReads: Reads[FirstChoice] = Json.reads[FirstChoice]
  val secondChoiceReads: Reads[SecondChoice] = Json.reads[SecondChoice]

  "A reads" should {
    implicit val choiceReads: Reads[Choice] = JsonDiscriminator.reads("type") {
      case "first" => firstChoiceReads.reads
      case "second" => secondChoiceReads.reads
    }
    "be able to read a first" in {
      Json.fromJson[Choice](Json.obj("type" -> "first", "first" -> "hello")) should ===(JsSuccess(FirstChoice("hello")))
    }
    "be able to read a second" in {
      Json.fromJson[Choice](Json.obj("type" -> "second", "second" -> "hello")) should ===(JsSuccess(SecondChoice("hello")))
    }
    "fail when there is no discriminator column" in {
      Json.fromJson[Choice](Json.obj("first"-> "hello")).isSuccess should ===(false)
    }
    "fail when there is an invalid discriminator column" in {
      Json.fromJson[Choice](Json.obj("type" -> "dunno", "first"-> "hello")).isSuccess should ===(false)
    }
    "fail when the discriminator column is not a string" in {
      Json.fromJson[Choice](Json.obj("type" -> Json.obj(), "first"-> "hello")).isSuccess should ===(false)
    }
  }

  "A writes" should {
    implicit val choiceWrites: OWrites[Choice] = JsonDiscriminator.writes("type") {
      case fc: FirstChoice => "first" -> firstChoiceWrites.writes(fc)
      case sc: SecondChoice => "second" -> secondChoiceWrites.writes(sc)
    }
    "be able to write a first" in {
      Json.toJson[Choice](FirstChoice("hello")) should === (Json.obj("type" -> "first", "first" -> "hello"))
    }
    "be able to write a second" in {
      Json.toJson[Choice](SecondChoice("hello")) should === (Json.obj("type" -> "second", "second" -> "hello"))
    }
  }

  "A format" should {
    implicit val choiceFormat: OFormat[Choice] = JsonDiscriminator.format("type") ({
      case "first" => firstChoiceReads.reads
      case "second" => secondChoiceReads.reads
    }, {
      case fc: FirstChoice => "first" -> firstChoiceWrites.writes(fc)
      case sc: SecondChoice => "second" -> secondChoiceWrites.writes(sc)
    })
    "be able to read a first" in {
      Json.fromJson[Choice](Json.obj("type" -> "first", "first" -> "hello")) should ===(JsSuccess(FirstChoice("hello")))
    }
    "be able to read a second" in {
      Json.fromJson[Choice](Json.obj("type" -> "second", "second" -> "hello")) should ===(JsSuccess(SecondChoice("hello")))
    }
    "fail when there is no discriminator column" in {
      Json.fromJson[Choice](Json.obj("first"-> "hello")).isSuccess should ===(false)
    }
    "fail when there is an invalid discriminator column" in {
      Json.fromJson[Choice](Json.obj("type" -> "dunno", "first"-> "hello")).isSuccess should ===(false)
    }
    "fail when the discriminator column is not a string" in {
      Json.fromJson[Choice](Json.obj("type" -> Json.obj(), "first"-> "hello")).isSuccess should ===(false)
    }
    "be able to write a first" in {
      Json.toJson[Choice](FirstChoice("hello")) should === (Json.obj("type" -> "first", "first" -> "hello"))
    }
    "be able to write a second" in {
      Json.toJson[Choice](SecondChoice("hello")) should === (Json.obj("type" -> "second", "second" -> "hello"))
    }
  }
}
